# SSC0740 - Sistemas Embarcados

- Professor: Vanderlei Bonato (a partir da aula 6); Eduardo do Valle Simões (até aula 5)
- Email: vbonato AT icmc.usp.br; Simoes AT icmc.usp.br
- Departamento de Sistemas de Computação – ICMC - USP
- Grupo de Sistemas Embarcados e Evolutivos
- Laboratório de Computação Reconfigurável

## Alunos de 2020 - Segundo semestre
- Caros Alunos: Essa semana NÁO teremos aula devido a SEMANA DA COMPUTAÇÃO (28/09/2020 )
- **Lista de Presença** - Assinar sempre a lista de presença no horário das aulas: https://docs.google.com/document/d/10DExJR7dmJ45HapQsP8Vsrmld7FGg8cySzrejY0fv2Y/edit?usp=sharing
- As aulas começam dia 24/08/20 e serão transmitidas pelo google Meet (sempre com o código **https://meet.google.com/jer-pkvg-jhi**) no horário normal da disciplina: seg 13:00-15:30
- **Experimentei o **Agendamento**, então não usar mais o codigo **SSC0740**
- As aulas serâo gravadas e disponibilisadas nessa plataforma
- Os alunos serão divididos em grupos de 3-4 alunos para implementação dos trabalhos práticos para as avaliações 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!

### Insira as informações do seu grupo no arquivo do GoogleDocs:
https://docs.google.com/document/d/17fJ-VJkK1Y4uvdb5NVrJz0VWbfsZ52nNLAHT80GP8IM/edit?usp=sharing

### Especificação do Trabalho 1
https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0740%20-%20Sistemas%20Embarcados/MaterialAulaDistancia/Trabalho1de2.pdf

**Resultado da Avaliação:**

Grupo 1: (10295645)(10262721)(10550218)
Mean Filter - Grupo 1 - 
https://github.com/skywardsc2/mean-filter-verilog
Nota: 9.0

Grupo 2: (8657904)(10408390)(10276953)
Detecação de bordas por operador Sobel - Grupo 2
https://github.com/marcelats/SistemasEmbarcados 
Nota: 6.5

Grupo 3: (11218991)(11219154)(10734432)
Detecão de contorno - Grupo 3 - https://github.com/Brenocq/FPGA-BorderDetection/blob/main/report.md
Nota: 9.0

Grupo 4: (10276911)(9805320)(9806933)
Redução de imagem - Grupo 4 - https://github.com/leonardoalvespaiva/SSC0740-reducao-imagem 
Nota: 9.0

Grupo 5: (8957197)(10284441)(10295670)
Filtro Passa-Alta - Grupo 5 -
https://github.com/LucasNT/SSC0740---Sistemas-Embarcados-filtro-passa-alta
Nota: 9.5

Grupo 6: (10734710)(10724239)(9898610)(10844313)
Nota: zero (não apresentaram)

Grupo 7: (10851707)(10734630)(10724413)(10260462)
Equalização de intensidade - Grupo 7 - https://github.com/matheustomieiro/equalizacao_intensidade 
Nota: 5.0


### Especificação do Trabalho 2
https://drive.google.com/file/d/1Z07U6O8DMdfA2u_I9ZV-y6WBQnlBa595/view?usp=sharing

**Resultado da Avaliação:**

Grupo 1 (10295645)(10262721)(10550218)
Classificação de gatos vs cachorros Android - Grupo 1 - https://github.com/skywardsc2/image-classification-optimization
Nota = 9.0

Grupo 2:(8657904)(10408390)(10276953)
Câmera de segurança - Grupo 2 - https://github.com/baldissin/tflite-embarcado
Nota = 6.5

Grupo 3:(11218991)(11219154)(10734432)
Classificação de gato vs cachorro - Grupo 3 - https://github.com/Brenocq/CatDogClassification
Nota = 8.0

Grupo 4:(10276911)(9805320)(9806933)
Classificação de imagens - Grupo 4 - https://github.com/leonardoalvespaiva/Classificacao-Imagens-TFLite
Nota = 8.0

Grupo 5: (8957197)(10284441)(10295670)
Análise e aplicação do modelo neural Yolov4 inseridos no contexto de sistemas embarcados - Grupo 5 -
https://github.com/aqilputi/trabalho-embarcados
Nota = 10.0


# Aulas a Distância 

## Aula 01
- Link para a Aula 01 (24/08/20) - https://drive.google.com/file/d/1JLa9CMkElZ9kHsjiMZXMXTzMCpMYDT98/view?usp=sharing

- Material (Fotos da Lousa, PDFs...) - https://gitlab.com/simoesusp/disciplinas/-/tree/master/SSC0740%20-%20Sistemas%20Embarcados/MaterialAulaDistancia

## Aula 02
- Link para a Aula 02 (31/08/20) - https://drive.google.com/file/d/18Bd8RHBv3BlStrO63eq4EBRriA-7z0cL/view?usp=sharing

## Aula 03
- Link para a Aula 03 (14/09/20) - https://drive.google.com/file/d/1wqrsON_G41rQNB9mjJiTQE4DvWfcnIrr/view?usp=sharing
- Embedded System Design Issues - https://users.ece.cmu.edu/~koopman/iccd96/iccd96.html

## Aula 04
- Link para a Aula (21/09/20) - https://drive.google.com/file/d/1q1vxk7jKeFKtUgVyeDir25fNgY9je3Tb/view?usp=sharing

## Aula 05
- Link para a Aula (05/10/20) - https://drive.google.com/file/d/1YEtVM-ul3UZ5XW1265cLERQ13Yqbjuyp/view?usp=sharing

## Aula 06
- Link para a Aula (19/10/20) - https://drive.google.com/file/d/1SsmYIEa1LuT61afuCLdL8rTjujzfzUpY/view?usp=sharing

**Introduction to FPGAs** 
- Intel Material: https://drive.google.com/drive/folders/1ReVUDOnTUtmLc3TOgtUZVWrBJunUz-dp?usp=sharing
- Video: [Introduction to the Acceleration Stack for Intel® Xeon® CPU with FPGA](https://www.youtube.com/watch?v=YlnuIH9e1KI)

## Aula 7
- Link para a Aula (26/10/20) - https://drive.google.com/file/d/1m1RYa8K60-H8jaNdDllSKbdyQtSjdMy-/view?usp=sharing

- Objetivo: Analisar as arquiteturas modernas de FPGAs da Intel e Xilinx; Entender as principais métricas de desempenho do hw
(https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0740%20-%20Sistemas%20Embarcados/MaterialAulaDistancia/Aula_26.10.pdf)

- Link para a especificacao do trabalho 1: https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0740%20-%20Sistemas%20Embarcados/MaterialAulaDistancia/Trabalho1de2.pdf

## Aula 8 - 09/11/2020
- Link para a Aula (09/11/20) - https://drive.google.com/file/d/1ec4RE07ktR5iYTHEDtk07b39PdBd4E2V/view?usp=sharing

- Incluir a composição dos grupos aqui: https://docs.google.com/document/d/17fJ-VJkK1Y4uvdb5NVrJz0VWbfsZ52nNLAHT80GP8IM/edit
-  Continuar conteúdo da aula 7: "Entender as principais métricas de desempenho do hw": https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0740%20-%20Sistemas%20Embarcados/MaterialAulaDistancia/Architecting_Speed.pdf
- Intro à Análise Temporal (material da Intel): https://drive.google.com/file/d/1T67Jo5G1B4NW8YMBY4ku6f5FzrtTzTcn/view?usp=sharing

## Aula 9 - 16/11/2020
- Link para a Aula (16/11/20) - https://drive.google.com/file/d/1vtx-a0vRp0g3YDbhyWvBwnldDsORLYQJ/view?usp=sharing

- Technologias recentes para integracao de Circuitos Integrados
https://gitlab.com/-/ide/project/simoesusp/disciplinas/edit/master/-/SSC0740%20-%20Sistemas%20Embarcados/MaterialAulaDistancia/Aula_16.11.pdf

## Aula 10 - 23/11/2020
 - Seminários do Trabalho 1: apresentação de 15 min.
 
 --Sobre entrega:
O trabalho deverá ser apresentado em forma de seminário e o código entregue ao professor juntamente com um breve relatório técnico (telas de entrada, de simulação, resultados do desempenho do Hardware, etc.). Cada grupo terá 15 minutos para a apresentação online do seminário durante o horário da aula. A entrega do relatorio e do código favor incluir no final do documento onde foi incluido o nome do grupo (https://docs.google.com/document/d/17fJ-VJkK1Y4uvdb5NVrJz0VWbfsZ52nNLAHT80GP8IM/edit?usp=sharing) a seguinte informação: [TÍTULO DO PROJETO - Grupo -  link do projeto (relatório + codigo) para o github/gitlab].

## Aula 11 - 30/11/2020
- Link para a Aula (30/11/20) - https://drive.google.com/file/d/13YxyJsCL0ngc4TL0iGlmGt9vhs6nA58d/view?usp=sharing

- Estudo de fluxos para embarcar redes neurais profundas
https://drive.google.com/file/d/1btmBn3PaB-hlai0BocJB5iFTEHYmZpzL/view?usp=sharing


## Aula 12 - 07/12/2020
- Link para a Aula (30/11/20) - (to come)

- TensorFlow Lite para sistemas embarcados e divulgação do Trabalho 2
https://drive.google.com/file/d/1Z5y8Vk4Y3MDzmvA5Q7egCs5Ko3ITLSqf/view?usp=sharing

## Aula 13 - 14/11/2020
-APENAS PARA ACOMPANHAMENTO DO PROJETO E TIRAS DÚVIDAS

## Aula 14 - 21/11/2020
- Semimários do trabalho 2


## Lista de projetos 2020

- Arrows (Bruno Pagno, João mello, Lucas Saliba): https://github.com/bruno-pagno/JogoSetas




## SSC0740 - Sistemas Embarcados

### Objetivos
Introduzir os Sistemas Embarcados e as suas áreas de aplicação. Integração entre sistemas de comunicação, multimídia e processamento incluindo dados em RF (radio freqüência) que continuam a expandir a complexidade dos sistemas embarcados, destacando exemplos existentes. Conceituar Sistemas DES (Distributed Embedded System) com grande número de elementos possuindo diferentes funcionalidades sendo considerados nós inteligentes, atuando através de sensores e atuadores, que configuram sistemas cada vez mais complexos e mais necessários às aplicações recentes.
 
 
### Programa Resumido
Introdução aos Sistemas Embarcados; Estudo das principais características dos elementos de computação tipo (DSP - Digital Signal Processing, processadores, FPGA e ASICs) voltados para aplicações embarcadas; Levantamento das limitações e capacidades do hardware e software destes elementos para a implementação de sistemas embarcados; Metodologias para comparar os resultados entre as diferentes tecnologias. Redes em sistemas de tempo real, escalonamento de mensagens, considerações sobre comunicação evento/tempo, impacto do meio físico, topologias e controle de acesso ao meio.
